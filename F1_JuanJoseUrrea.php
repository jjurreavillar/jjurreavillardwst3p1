<?php
require("verificar.php");
if (isset($_POST['dato']) && isset($_POST['tipodato']))
    $infoDato = VerificarIntegridadDato($_POST['dato'], $_POST['tipodato']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Formulario para comprobar tipo de datos (Ampliación - 3)</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>FORMULARIO PARA COMPROBAR TIPO DE DATOS</h1>
<?php
if (!isset($infoDato))
{
?>
        <label class="aviso">Error: dato no introducido o tipo de dato no seleccionado.</label>
<?php
}
else if ($infoDato[0])
{
?>
        <label>El dato <?= $infoDato[1] ?> era del tipo de dato seleccionado, <?= $infoDato[2] ?></label>

<?php
}
else
{
?>
        <label class="aviso">El dato introducido, <?= $infoDato[1] ?> no es del tipo de dato seleccionado, sino <?= $infoDato[2] ?>.</label>
<?php
}
?>
        <div class="der">
            <a href="F1_JuanJoseUrrea.html">Volver al formulario</a>
        </div>
    </body>
</html>
