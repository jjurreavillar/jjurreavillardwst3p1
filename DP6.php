<?php
require("verificar.php");
if (isset($_POST['edad']))
    $edad = Verificar($_POST['edad']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos personales 6 (formulario). Controles en formularios. Ejercicios.</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>DATOS PERSONALES 6 (FORMULARIO)</h1>
<?php
if (!isset($edad))
{
?>
        <label class="aviso">Error: no se ha introducido ninguna edad</label>
<?php
}
else if (!$edad)
{
?>
        <label class="aviso">Error: La edad introducida no es válida</label>
<?php
}
else
{
?>
        <label>Edad: <?= $edad ?></label>
<?php
}
?>
        <div class="der">
            <a href="DP6.html">Volver al formulario</a>
        </div>
    </body>
</html>
