<?php
require("verificar.php");
if (isset($_POST['nombre']))
    $nombre = Verificar($_POST['nombre']);
if (isset($_POST['apellidos']))
    $apellidos = Verificar($_POST['apellidos']);
if (isset($_POST['edad']))
    $edad = Verificar($_POST['edad']);
if (isset($_POST['peso']))
    $peso = Verificar($_POST['peso'], true);
if (isset($_POST['sexo']))
    $sexo = Verificar($_POST['sexo']);
if (isset($_POST['estadocivil']))
    $estadocivil = Verificar($_POST['estadocivil']);
if (isset($_POST['aficiones']))
    foreach ($_POST['aficiones'] as $indice => $aficion)
        $aficiones[$indice] = Verificar($aficion);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos personales 7 (formulario). Controles en formularios. Ejercicios.</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>DATOS PERSONALES 7 (FORMULARIO)</h1>
<?php
if (!isset($nombre))
{
?>
        <label class="aviso">Error: nombre no introducido.</label>
<?php
}
else if (!$nombre)
{
?>
        <label class="aviso">Error: el nombre introducido no es válido.</label>
<?php
}
else
{
?>
        <label>Nombre: <?= $nombre ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($apellidos))
{
?>
        <label class="aviso">Error: apellidos no introducidos.</label>
<?php
}
else if (!$apellidos)
{
?>
        <label class="aviso">Error: los apellidos introducidos no son válidos.</label>
<?php
}
else
{
?>
        <label>Apellidos: <?= $apellidos ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($edad))
{
?>
        <label class="aviso">Error: no se ha introducido ninguna edad.</label>
<?php
}
else if (!$edad)
{
?>
        <label class="aviso">Error: La edad introducida no es válida.</label>
<?php
}
else
{
?>
        <label>Edad: <?= $edad ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($peso))
{
?>
        <label class="aviso">Error: peso no introducido.</label>
<?php
}
else if (!$apellidos)
{
?>
        <label class="aviso">Error: el peso introducido no es válido.</label>
<?php
}
else
{
?>
        <label>Peso: <?= $peso ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($sexo))
{
?>
        <label class="aviso">Error: sexo no seleccionado.</label>
<?php
}
else
{
?>
        <label>Sexo: <?= $sexo ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($estadocivil))
{
?>
        <label class="aviso">Error: estado civil no seleccionado.</label>
<?php
}
else
{
?>
        <label>Estado civil: <?= $estadocivil ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($aficiones))
{
?>
        <label>Aficiones: ninguna</label>
<?php
}
else
{
?>
        <label>Aficiones: <?= ucfirst(join(", ", $aficiones)) ?></label>
<?php
}
?>
        <div class="der">
            <a href="DP7.html">Volver al formulario</a>
        </div>
    </body>
</html>
