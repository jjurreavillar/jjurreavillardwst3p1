<?php
require("verificar.php");
$nombre = Verificar($_POST['nombre']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos personales 2 (formulario). Controles en formularios. Ejercicios.</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>DATOS PERSONALES 2 (FORMULARIO)</h1>
<?php
if (!isset($nombre))
{
?>
        <label class="aviso">Error: nombre no introducido.</label>
<?php
}
else if (!$nombre)
{
?>
        <label class="aviso">Error: el nombre introducido no es válido.</label>
<?php
}
else
{
?>
        <label>Nombre: <?= $nombre ?></label>
<?php
}
?>
        <div class="der">
            <a href="DP2.html">Volver al formulario</a>
        </div>
    </body>
</html>
