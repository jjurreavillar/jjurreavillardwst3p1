<?php
require("verificar.php");
$nombre = Verificar($_POST['nombre']);
$apellidos = Verificar($_POST['apellidos']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos personales 3 (formulario). Controles en formularios. Ejercicios.</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>DATOS PERSONALES 3 (FORMULARIO)</h1>
<?php
if (!isset($nombre))
{
?>
        <label class="aviso">Error: nombre no introducido.</label>
<?php
}
else if (!$nombre)
{
?>
        <label class="aviso">Error: el nombre introducido no es válido.</label>
<?php
}
else
{
?>
        <label>Nombre: <?= $nombre ?></label>
<?php
}
?>
        <br>
        <br>
<?php
if (!isset($apellidos))
{
?>
        <label class="aviso">Error: apellidos no introducidos.</label>
<?php
}
else if (!$apellidos)
{
?>
        <label class="aviso">Error: los apellidos introducidos no son válidos.</label>
<?php
}
else
{
?>
        <label>Apellidos: <?= $apellidos ?></label>
<?php
}
?>
        <div class="der">
            <a href="DP3.html">Volver al formulario</a>
        </div>
    </body>
</html>
