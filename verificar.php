<?php
$patronAlfanumerico = "/[^a-z\d]+/i";
$patronEmail = "/^[\w\.\-_]+@[\w\.\-_]+\.[a-z]+$/i";
$patronHexadecimal = "/^0x[\da-f]+$/i";
$patronNIF = "/^[a-z]?\d+[a-z]$/i";
$patronNumerico = "/[^\d]+/";
$patronSoloLetras = "/[^a-z]+/i";

function Verificar($dato, $soloNum = false)
{
    $dato = trim($dato);
    
    if (empty($dato))
        return null;
    
    $dato = htmlspecialchars(strip_tags($dato));

    // Ninguno de los valores a verificar debería ser mixto
    if (preg_match("/[a-z\s]+[\d]+|[\d]+[a-z\s]+/i", $dato) != 0)
        return false;

    // Solo números
    if ($soloNum && preg_match("/[^\d]+/i", $dato) != 0)
        return false;

    return $dato;
}

function DevolverTipoDato($dato)
{
    global $patronAlfanumerico, $patronEmail, $patronHexadecimal;
    global $patronNIF, $patronNumerico, $patronSoloLetras;

    // El orden de las comprobaciones es importante para evitar falsos positivos
    if (preg_match($patronEmail, $dato) == 1)
        return array(false, $dato, "email");
    if (preg_match($patronHexadecimal, $dato) == 1)
        return array(false, $dato, "hexadecimal");
    if (preg_match($patronNIF, $dato) == 1)
        return array(false, $dato, "NIF");
    if (preg_match($patronSoloLetras, $dato) == 0)
        return array(false, $dato, "sólo letras");
    if (preg_match($patronNumerico, $dato) == 0)
        return array(false, $dato, "numérico");
    if (preg_match($patronAlfanumerico, $dato) == 0)
        return array(false, $dato, "alfanumérico");

    return array(false, $dato, "desconocido");
}

function VerificarIntegridadDato($dato, $tipoDato)
{
    global $patronAlfanumerico, $patronEmail, $patronHexadecimal;
    global $patronNIF, $patronNumerico, $patronSoloLetras;

    $dato = trim($dato);
    $dato = htmlspecialchars(strip_tags($dato));

    $tipoDato = Verificar($tipoDato);

    switch ($tipoDato)
    {
        // Sin break porque usamos return
        case "alfanumérico":
            if (preg_match($patronAlfanumerico, $dato) != 0)
                return DevolverTipoDato($dato);
            else
                return array(true, $dato, "alfanumérico");

        case "email":
            if (preg_match($patronEmail, $dato) != 1)
                return DevolverTipoDato($dato);
            else
                return array(true, $dato, "email");

        case "hexadecimal":
            if (preg_match($patronHexadecimal, $dato) != 1)
                return DevolverTipoDato($dato);
            else
                return array(true, $dato, "hexadecimal");

        case "NIF":
            if (preg_match($patronNIF, $dato) != 1)
                return DevolverTipoDato($dato);
            else
                return array(true, $dato, "NIF");

        case "numérico":
            if (preg_match($patronNumerico, $dato) != 0)
                return DevolverTipoDato($dato);
            else
                return array(true, $dato, "numérico");

        case "sololetras":
            if (preg_match($patronSoloLetras, $dato) != 0)
                return DevolverTipoDato($dato);
            else
                return array(true, $dato, "sólo letras");

        default:
            return null;
    }
}
?>
