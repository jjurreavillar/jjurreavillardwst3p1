<?php
require("verificar.php");
if (isset($_POST['aficiones']))
    foreach ($_POST['aficiones'] as $indice => $aficion)
        $aficiones[$indice] = Verificar($aficion);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos personales 5 (formulario). Controles en formularios. Ejercicios.</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>DATOS PERSONALES 5 (FORMULARIO)</h1>
<?php
if (!isset($aficiones))
{
?>
        <label>Aficiones: ninguna</label>
<?php
}
else
{
?>
        <label>Aficiones: <?= ucfirst(join(", ", $aficiones)) ?></label>
<?php
}
?>
        <div class="der">
            <a href="DP5.html">Volver al formulario</a>
        </div>
    </body>
</html>
