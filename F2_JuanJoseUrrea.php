<?php
require("verificar.php");
if (isset($_POST['usuario']))
    $usuario = VerificarIntegridadDato($_POST['usuario'], "alfanumérico");
if (isset($_POST['nombre']))
    $nombre = Verificar($_POST['nombre']);
if (isset($_POST['email']))
    $email = VerificarIntegridadDato($_POST['email'], "email");
if (isset($_POST['pw']) && isset ($_POST['confirmpw']))
{
    $pw = VerificarIntegridadDato($_POST['pw'], "alfanumérico");
    $confirmpw = VerificarIntegridadDato($_POST['confirmpw'], "alfanumérico");
}
if (isset($_POST['terminoscb']))
    $terminoscb = Verificar($_POST['terminoscb']);
if (isset($_POST['ventaalmacb']))
    $ventaalmacb = Verificar($_POST['ventaalmacb']);

$parametrosValidos = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Formulario para dar de alta a un usuario (Ampliación - 4)</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>FORMULARIO PARA DAR DE ALTA A UN USUARIO</h1>
<?php
if (!isset($usuario))
    echo '<label class="aviso">El nombre de usuario no ha sido introducido.</label>';
else if (!$usuario[0])
    echo '<label class="aviso">El nombre de usuario introducido no es válido.</label>';
else
    $parametrosValidos++;
if (!isset($nombre))
    echo '<label class="aviso">El nombre no ha sido introducido.</label>';
else if (!$nombre[0])
    echo '<label class="aviso">El nombre introducido no es válido.</label>';
else
    $parametrosValidos++;
if (!isset($email))
    echo '<label class="aviso">El email no ha sido introducido.</label>';
else if (!$email[0])
    echo '<label class="aviso">El email introducido no es válido.</label>';
else
    $parametrosValidos++;
if (!isset($pw) || !isset($confirmpw))
    echo '<label class="aviso">La contraseña no ha sido introducida.</label>';
else if (!$pw[0] || !$pw[0])
    echo '<label class="aviso">La contraseña introducida no es válida.</label>';
else if (strcmp($pw[1], $confirmpw[1]) != 0)
    echo '<label class="aviso">La contraseña introducida no coincide.</label>';
else
    $parametrosValidos++;
if (isset($terminoscb))
    $parametrosValidos++;
if (isset($ventaalmacb))
    $parametrosValidos++;

if ($parametrosValidos == 6)
{
    // $_FILES['archivo']['type'] recibe el tipo MIME del cliente, por lo que puede no ser el correcto.
    // getimagesize devuelve FALSE para archivos que no son imágenes.
    if (is_uploaded_file($_FILES['archivo']['tmp_name']) && $_FILES['archivo']['size'] > 0 && $_FILES['archivo']['size'] <= 153600 && getimagesize($_FILES['archivo']['tmp_name']))
    {
        $directorio = "subidas";
        $archivoTemporal = $_FILES['archivo']['tmp_name'];
        $archivo = $directorio . "/" . time() . "_" . $_FILES['archivo']['name'];

        if (!is_dir($directorio) && !file_exists($directorio))
            mkdir($directorio);

        $movido = move_uploaded_file($archivoTemporal, $archivo);
    }

    if (isset($movido) && $movido)
    {
        $fs = fopen("datos.txt", "at");
        fwrite($fs, "Nombre de usuario: " . $usuario[1] . "\n");
        fwrite($fs, "Nombre: " . $nombre . "\n");
        fwrite($fs, "Email: " . $email[1] . "\n");
        fwrite($fs, "Contraseña: " . password_hash($pw[1], PASSWORD_DEFAULT) . "\n");
        fwrite($fs, "Foto: " . $archivo . "\n");
        fwrite($fs, "Acepta los términos: " . isset($terminoscb) . "\n");
        fwrite($fs, "Acepta vender su alma: " . isset($ventaalmacb) . "\n");
        fclose($fs);
        echo '<label>El usuario ha sido dado de alta correctamente.</label>';
    }
    else
    {
        if ($_FILES['archivo']['size'] == 0 || $_FILES['archivo']['size'] > 153600)
            echo '<label class="aviso">El archivo seleccionado no cumple con los límites de tamaño establecidos.</label>';
        else if (!getimagesize($_FILES['archivo']['tmp_name']))
            echo '<label class="aviso">El archivo seleccionado no es un tipo de imagen admitido.</label>';
        else
            echo '<label class="aviso">El archivo seleccionado no ha sido subido correctamente: ' . $_FILES['archivo']['error'] . '.</label>';
    }
}
?>
        <div class="der">
            <a href="F2_JuanJoseUrrea.html">Volver al formulario</a>
        </div>
    </body>
</html>
