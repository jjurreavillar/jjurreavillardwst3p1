<?php
require("verificar.php");
if (isset($_POST['sexo']))
    $sexo = Verificar($_POST['sexo']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos personales 4 (formulario). Controles en formularios. Ejercicios.</title>
        <link rel="stylesheet" href="estilo.css">
    </head>
    <body>
        <h1>DATOS PERSONALES 4 (FORMULARIO)</h1>
<?php
if (!isset($sexo))
{
?>
        <label class="aviso">Error: sexo no seleccionado.</label>
<?php
}
else
{
?>
        <label>Sexo: <?= $sexo ?></label>
<?php
}
?>
        <div class="der">
            <a href="DP4.html">Volver al formulario</a>
        </div>
    </body>
</html>
